# Markdown Cheatsheet
<br>
<br>

Markdown:
```
  ![Header Image](picture.png)
```

Example:

  ![Header Image](picture.png)

Markdown:

```
Line1:
  =========

Line2:
  ---------

 # Heading1
 ## Heading 2
 ### Heading 3
 #### Heading 4
 ```

Example:

Line1:
  =========

Line2:
  ---------

# Heading 1
## Heading 2
### Heading 3
#### Heading 4

<br>
<br>

# Text Decoration and Links
<br>
<br>

Markdown:

```
 _Emphasized text_

 *Bold text*

 **Strong text**

 ~~Strikethrough text~~

 [Text of the link](README.md)

 [Link to an anchor](#heading-1)

 [Link with Tooltip](#heading-1 "Heading Section")
```

Example:

_Emphasized text_

*Bold text*

**String text**

~~Strikethrough text~~

 [Text of the link](README.md)

 [Link to an anchor](#heading-1)

 [Link with Tooltip](#heading-1 "Heading Section")

Markdown:

```
<kbd>⌘A</kbd>
```

Example:

<kbd>⌘A</kbd>

Markdown:

```
 > Block quote
 >> Nested quote
 ```
 
Example:

 > Block quote
 >> Nested quote

Markdown:

<pre>
Code:

```c
  void print(char *str) {
    puts(str);
  }
```
</pre>

Code:

```c
  void print(char *str) {
    puts(str);
  }
```
<br>
<br>

# Lists and CheckLists
<br>
<br>

Markdown:

```
 * List item1
 * List item2
    * Nested item
 * List item3

 OR

 - List item1
 - List item2
    - Nested item
 - List item3
```

Example:

 * List item1
 * List item2
    * Nested item
 * List item3

Markdown:

```
  1. Numbered item1
  2. Numbered item2
    1. Nested item1
    2. Nested item2
  3. Numbered item3
```

Example:

  1. Numbered item1
  2. Numbered item2
    1. Nested item1
    2. Nested item2
  3. Numbered item3

Markdown:

```
  - [ ] Task to complete
    - [ ] Subtask
  - [x] Completed task
```

Example:

  - [ ] Task to complete
    - [ ] Subtask
  - [x] Completed task

<br>
<br>

# Tables and HTML
<br>
<br>

Markdown:

```
 Table Header | Table Header
------------- | -------------
Cell Content  | Cell Content
Cell Content  | Cell Content
```

Example:

 Table Header | Table Header
------------- | -------------
Cell Content  | Cell Content
Cell Content  | Cell Content


Markdown:

```
To escape a sequence of ```  ```  you can use <pre></pre> html tag
Also <center></center> and <br> work as usual.
```

# License

GPLv2

```
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 Author: g0tsu
 Email:  g0tsu at dnmx.0rg
```
